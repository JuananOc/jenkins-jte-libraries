void call(){
    stage("Maven: Build"){
        println "build from the maven library"
        println "MAVEN: before ${config}"

        config.put("maven_param1", "HOLA")
        config.put("maven_param2", "MUNDO")

        println "MAVEN: After ${config}"
    }

    return ["HOLA", "MUNDO"]
}
